FROM ubuntu:18.04
RUN apt-get update && apt-get install -y 
ADD ./html5up/ /var/www/html/
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]